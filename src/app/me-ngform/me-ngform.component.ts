import { Component, OnInit, Input } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';

@Component({
  selector: 'app-me-ngform',
  templateUrl: './me-ngform.component.html',
  styleUrls: ['./me-ngform.component.css']
})

export class MeNgformComponent implements OnInit {
  ngOnInit() {
  }
  
  onSubmit(form: any): void {
    console.log('you submitted value: ', form);
  }

}
