/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { MeNgformComponent } from './me-ngform.component';

describe('MeNgformComponent', () => {
  let component: MeNgformComponent;
  let fixture: ComponentFixture<MeNgformComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MeNgformComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MeNgformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
